<!-- CARDS -->
@section('title','Import')
@extends('admin.layout')
@section('sidebar')
    @include('admin.sidebar')
@endsection
@section('content')
    <!-- MAIN CONTENT
    ================================================== -->


        <!-- HEADER -->

        <!-- CARDS -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-body">

                            <form action="/admin/import" method="post" enctype="multipart/form-data">
                                @csrf()
                                <input type="file" name="file"><br/>
                                <button type="submit" class="btn btn-primary" style="margin-top: 40px">Submit</button>
                            </form>
                            @if (session('status'))
                                <div class="alert alert-success">
                                    <?php var_dump(session('status')); ?>
                                </div>
                            @endif

                        </div>
                    </div>

                </div>
            </div> <!-- / .row -->

        </div> <!-- / .container-fluid -->


@endsection