<!-- CARDS -->
@extends('admin.layout')
@section('sidebar')
 @include('admin.sidebar')
@endsection
@section('content')
 <!-- MAIN CONTENT



  <!-- CARDS -->
  <div class="container">
   <div class="row">
    <div class="col-12">
     <div class="card">
      <div class="card-header">
       <div class="row align-items-center">
        <div class="col">

         <!-- Title -->
         <h4 class="card-header-title">
          Projects
         </h4>

        </div>
        <div class="col-auto">

         <!-- Button -->
         <a href="/project/new" class="btn btn-sm btn-white">
          Add new
         </a>

        </div>
       </div> <!-- / .row -->
      </div>
      <div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
       <table class="table table-sm table-nowrap card-table">
        <thead>
        <tr>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-project">
           ID
          </a>
         </th>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-status">
           Name
          </a>
         </th>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-progress">
           Description
          </a>
         </th>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-date">
           FCM Server API key
          </a>
         </th>
         <th class="text-right">
          Team
         </th>
         <th></th>
        </tr>
        </thead>
        <tbody class="list">
        <tr>
         <td class="goal-project">
          Update the API
         </td>
         <td class="goal-status">
          <span class="text-warning">●</span> In progress
         </td>
         <td class="goal-progress">
          55%
         </td>
         <td class="goal-date">
          <time datetime="2018-10-24">07/24/18</time>
         </td>
         <td class="text-right">
          <div class="avatar-group">
           <a href="profile-posts.html" class="avatar avatar-xs" data-toggle="tooltip" title="" data-original-title="Dianna Smiley">
            <img src="assets/img/avatars/profiles/avatar-1.jpg" class="avatar-img rounded-circle border border-white" alt="...">
           </a>
           <a href="profile-posts.html" class="avatar avatar-xs" data-toggle="tooltip" title="" data-original-title="Ab Hadley">
            <img src="assets/img/avatars/profiles/avatar-2.jpg" class="avatar-img rounded-circle border border-white" alt="...">
           </a>
           <a href="profile-posts.html" class="avatar avatar-xs" data-toggle="tooltip" title="" data-original-title="Adolfo Hess">
            <img src="assets/img/avatars/profiles/avatar-3.jpg" class="avatar-img rounded-circle border border-white" alt="...">
           </a>
           <a href="profile-posts.html" class="avatar avatar-xs" data-toggle="tooltip" title="" data-original-title="Daniela Dewitt">
            <img src="assets/img/avatars/profiles/avatar-4.jpg" class="avatar-img rounded-circle border border-white" alt="...">
           </a>
          </div>
         </td>
         <td class="text-right">
          <div class="dropdown">
           <a href="#!" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fe fe-more-vertical"></i>
           </a>
           <div class="dropdown-menu dropdown-menu-right">
            <a href="#!" class="dropdown-item">
             Action
            </a>
            <a href="#!" class="dropdown-item">
             Another action
            </a>
            <a href="#!" class="dropdown-item">
             Something else here
            </a>
           </div>
          </div>
         </td>
        </tr>
        </tbody>
       </table>
      </div>
     </div>
    </div>
  </div> <!-- / .container-fluid -->
 </div> <!-- / .main-content -->

@endsection