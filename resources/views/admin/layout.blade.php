<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <link rel="shortcut icon" href="{{asset('assets/img/avatars/profiles/avatar-1.jpg')}}" type="image/png">
    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('/assets/fonts/feather/feather.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/libs/highlight/styles/vs2015.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/libs/quill/dist/quill.core.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/libs/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/libs/flatpickr/dist/flatpickr.min.css')}}">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('/assets/css/theme.min.css')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('css')
    <title>@yield('title')</title>
</head>
<body>
@yield('sidebar')
<div class="main-content" style="padding-top: 36px">
    @yield('content')
</div>
<!-- JAVASCRIPT
================================================== -->
<!-- Libs JS -->
<script src="{{asset('/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/assets/libs/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{asset('/assets/libs/chart.js/Chart.extension.min.js')}}"></script>
<script src="{{asset('/assets/libs/highlight/highlight.pack.min.js')}}"></script>
<script src="{{asset('/assets/libs/flatpickr/dist/flatpickr.min.js')}}"></script>
<script src="{{asset('/assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js')}}"></script>
<script src="{{asset('/assets/libs/list.js/dist/list.min.js')}}"></script>
<script src="{{asset('/assets/libs/quill/dist/quill.min.js')}}"></script>
<script src="{{asset('/assets/libs/dropzone/dist/min/dropzone.min.js')}}"></script>
<script src="{{asset('/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<!-- Theme JS -->
<script src="{{asset('/assets/js/main.js')}}"></script>
<script src="{{asset('/assets/js/theme.min.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('script')
</body>
</html>