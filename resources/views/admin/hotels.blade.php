@extends('admin.layout')
@section('title','Hotels')
@section('sidebar')
 @include('admin.sidebar')
@endsection
@section('content')
 <!-- MAIN CONTENT
    ================================================== -->


  <!-- HEADER -->

  <!-- CARDS -->
  <div class="container-fluid">
   <div class="row">
    <div class="col-12">

     <div class="card" data-toggle="lists" data-lists-values="[&quot;name&quot;]">
      <div class="card-header">
       <div class="row align-items-center">
        <div class="col">

         <!-- Title -->
         <h4 class="card-header-title">
          Hotels
         </h4>

        </div>
        <div class="col-auto">

         <!-- Dropdown -->
         <div class="dropdown">

          <!-- Toggle -->
          <a href="#!" class="small text-muted dropdown-toggle" data-toggle="dropdown">
           Sort order
          </a>

          <!-- Menu -->
          <div class="dropdown-menu">
           <a class="dropdown-item sort" data-sort="name" href="#!">
            Asc
           </a>
           <a class="dropdown-item sort" data-sort="name" href="#!">
            Desc
           </a>
          </div>

         </div>

        </div>
        <div class="col-auto">

         <!-- Button -->
         <a href="#!" class="btn btn-sm btn-primary">
          Import
         </a>

        </div>
       </div> <!-- / .row -->
      </div>
      <div class="card-header">
       <div class="row">
        <div class="col-12">

         <!-- Form -->
         <form>
          <div class="input-group input-group-flush input-group-merge">
           <input type="search" class="form-control form-control-prepended search" placeholder="Search">
           <div class="input-group-prepend">
            <div class="input-group-text">
             <span class="fe fe-search"></span>
            </div>
           </div>
          </div>
         </form>

        </div>
       </div> <!-- / .row -->
      </div>
      <div class="card-body">

       <!-- List -->
       <ul class="list-group list-group-lg list-group-flush list my--4">
        @foreach($hotels as $key => $hotel)
        <li class="list-group-item px-0">
         <div class="row align-items-center">
          <div class="col-auto">
           <!-- Avatar -->
           <a href="#!" class="avatar avatar-lg">
               <?php
               if(count($hotel->images) > 0){
                   foreach ($hotel->images as $k => $value){
                       echo '<img src="'.$value->name.'" alt="" class="avatar-img rounded">';
                       break;
                   }
               }else{?>
                   <img src="{{asset('assets/img/files/file-1.jpg')}}" alt="" class="avatar-img rounded">
                <?php
               }
               ?>
           </a>
          </div>
          <div class="col ml--2">
           <!-- Title -->
           <h4 class="card-title mb-1 name">
            <a href="#!">{{ $hotel->name }}</a>
           </h4>
           <!-- Text -->
           <p class="card-text small text-muted mb-1">
            rate : {{ $hotel->star_rate }}
           </p>
           <!-- Time -->
           <p class="card-text small text-muted">
            {{ $hotel->address }}
           </p>
          </div>
          <div class="col-auto">
           <!-- Button -->
           <a href="#!" class="btn btn-sm btn-white d-none d-md-inline-block">
            Detail
           </a>
          </div>
          <div class="col-auto">
           <!-- Dropdown -->
           <div class="dropdown">
            <a href="#!" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <i class="fe fe-more-vertical"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
             <a href="#!" class="dropdown-item">
              Action
             </a>
             <a href="#!" class="dropdown-item">
              Another action
             </a>
             <a href="#!" class="dropdown-item">
              Something else here
             </a>
            </div>
           </div>
          </div>
         </div> <!-- / .row -->
        </li>
        @endforeach
       </ul>
      </div>
     </div>

    </div>
   </div> <!-- / .row -->

  </div> <!-- / .container-fluid -->


@endsection