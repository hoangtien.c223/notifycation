<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" style="padding-top: 0px">
 <div class="container-fluid">

  <!-- Toggler -->

  <div class="navbar-user mt-auto d-none d-md-flex">

   <!-- Icon -->
   <a href="#sidebarModalActivity" class="text-muted" data-toggle="modal">
              <span class="icon">
                <i class="fe fe-bell"></i>
              </span>
   </a>

   <!-- Dropup -->
   <div class="dropup">

    <!-- Toggle -->
    <a href="#!" id="sidebarIconCopy" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     <div class="avatar avatar-sm avatar-online">
      <img src="{{asset('assets/img/avatars/profiles/avatar-1.jpg')}}" class="avatar-img rounded-circle" alt="...">
     </div>
    </a>

    <!-- Menu -->
    <div class="dropdown-menu" aria-labelledby="sidebarIconCopy">
     <a href="profile-posts.html" class="dropdown-item">Profile</a>
     <a href="settings.html" class="dropdown-item">Settings</a>
     <hr class="dropdown-divider">
     <a href="sign-in.html" class="dropdown-item">Logout</a>
    </div>

   </div>

   <!-- Icon -->
   <a href="#sidebarModalSearch" class="text-muted" data-toggle="modal">
              <span class="icon">
                <i class="fe fe-search"></i>
              </span>
   </a>

  </div>
  <!-- Brand -->

  <!-- User (xs) -->
  <div class="navbar-user d-md-none">

   <!-- Dropdown -->
   <div class="dropdown">

    <!-- Toggle -->
    <a href="#!" id="sidebarIcon" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     <div class="avatar avatar-sm avatar-online">
      <!--<img src="{{ asset('assets/img/avatars/profiles/avatar-1.jpg') }}" class="avatar-img rounded-circle">-->
     </div>
    </a>

    <!-- Menu -->
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sidebarIcon">
     <a href="profile-posts.html" class="dropdown-item">Profile</a>
     <a href="settings.html" class="dropdown-item">Settings</a>
     <hr class="dropdown-divider">
     <a href="sign-in.html" class="dropdown-item">Logout</a>
    </div>

   </div>

  </div>

  <!-- Collapse -->
  <div class="collapse navbar-collapse" id="sidebarCollapse">

   <!-- Navigation -->
   <ul class="navbar-nav">
    <li class="nav-item">
     <a class="nav-link active" href="{{url("project")}}"  role="button" aria-expanded="false" aria-controls="sidebarPages">
      <i class="fe fe-file"></i> List Project
     </a>
    </li>
    <!--<li class="nav-item">
     <a class="nav-link" href="/admin/import"  role="button" aria-expanded="false" aria-controls="sidebarAuth">
      <i class="fe fe-user"></i> Import
     </a>
    </li>
    <li class="nav-item dropdown">
     <a class="nav-link" href="/admin/map"  role="button" aria-expanded="false" aria-controls="sidebarLayouts">
      <i class="fe fe-layout"></i> Map
     </a>
    </li>-->
   </ul>

   <!-- Divider -->
   <hr class="my-3">

  </div> <!-- / .navbar-collapse -->

 </div> <!-- / .container-fluid -->
</nav>