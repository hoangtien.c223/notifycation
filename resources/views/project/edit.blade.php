<!-- CARDS -->
@extends('admin.layout')
@section('sidebar')
 @include('admin.sidebar')
@endsection
@section('content')
 <!-- MAIN CONTENT



  <!-- CARDS -->
  <div class="container">
   <div class="row">
    <div class="col-12">
     <div class="card">
      <div class="card-body">
       <form method="post" action="{{url("/project/edit/$project->key")}}">
        @csrf
        <div class="form-group">
         <label for="">Project name</label>
         <input type="hidden" value="{{$project->key}}" name="key">
         <input type="text" name="name" class="form-control" value="{{$project->name}}" required>
        </div>
        <div class="form-group">
         <label for="">Project description</label>
         <textarea name="description" id="" cols="30" rows="5" class="form-control">{{$project->description}}</textarea>
        </div>
        <div class="form-group">
         <label for="">FCM Server API key</label>
         <textarea name="server_api_key" id="" cols="30" rows="5" class="form-control" required>{{$project->server_api_key}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
       </form>

      </div>
     </div>
    </div>
  </div> <!-- / .container-fluid -->
 </div> <!-- / .main-content -->

@endsection