<!-- CARDS -->
@extends('admin.layout')
@section('sidebar')
 @include('admin.sidebar')
@endsection
@section('content')
 <!-- MAIN CONTENT



  <!-- CARDS -->
  <div class="container">
   <div class="row">
    <div class="col-12">
     <div class="card">
      <div class="card-header">
       <div class="row align-items-center">
        <div class="col">

         <!-- Title -->
         <h4 class="card-header-title">
          Projects
         </h4>

        </div>
        <div class="col-auto">

         <!-- Button -->
         <a href="{{url('/project/new')}}" class="btn btn-sm btn-white">
          Add new
         </a>

        </div>
       </div> <!-- / .row -->
      </div>
      <div class="table-responsive mb-0" data-toggle="lists" data-lists-values="[&quot;goal-project&quot;, &quot;goal-status&quot;, &quot;goal-progress&quot;, &quot;goal-date&quot;]">
       <table class="table table-sm table-nowrap card-table">
        <thead>
        <tr>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-project">
           Project Key
          </a>
         </th>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-status">
           Name
          </a>
         </th>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-progress">
           Description
          </a>
         </th>
         <th>
          <a href="#" class="text-muted sort" data-sort="goal-date">
           FCM Server API key
          </a>
         </th>
         <th class="text-right">
         </th>
         <th class="text-right">
         </th>
         <!--<th></th>-->
        </tr>
        </thead>
        <tbody class="list">
        @foreach($projects as $key => $value)
        <tr>
         <td class="goal-project">
          {{ $value->key }}
         </td>
         <td class="goal-status">
          <span class="text-success">●</span> {{ $value->name }}
         </td>
         <td class="goal-progress">
          {{ $value->description }}
         </td>
         <td class="goal-date">
          <input type="text" value="{{ $value->server_api_key }}" class="form-control">
         </td>
         <td class="text-center">
          <div class="avatar-group">
           <a href="{{url("/project/edit/$value->key")}}" class="btn btn-sm btn-primary">
            edit
           </a>
          </div>
         </td>
         <td class="text-center">
          <a href="{{url("/project/delete/$value->key")}}" class="btn btn-sm btn-danger">
           delete
          </a>
         </td>
         <!--<td class="text-right">
          <div class="dropdown">
           <a href="#!" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fe fe-more-vertical"></i>
           </a>
           <div class="dropdown-menu dropdown-menu-right">
            <a href="#!" class="dropdown-item">
             Action
            </a>
            <a href="#!" class="dropdown-item">
             Another action
            </a>
            <a href="#!" class="dropdown-item">
             Something else here
            </a>
           </div>
          </div>
         </td>-->
        </tr>
         @endforeach
        </tbody>
       </table>
      </div>
     </div>
    </div>
  </div> <!-- / .container-fluid -->
 </div> <!-- / .main-content -->

@endsection