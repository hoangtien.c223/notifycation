<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::middleware('auth')->group(function(){
    //Route::group(['prefix' => 'backend'],function(){
        Route::get('/project', 'ProjectController@index');
        Route::get('/project/new', 'ProjectController@create');
        Route::post('/project/new', 'ProjectController@store');
        Route::get('/project/edit/{key}', 'ProjectController@edit');
        Route::post('/project/edit/{key}', 'ProjectController@update');
        Route::get('/project/delete/{key}', 'ProjectController@delete');
    //});
});

