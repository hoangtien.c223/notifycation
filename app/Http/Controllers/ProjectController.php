<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;


class ProjectController extends Controller
{
    public function index(){
        $project = Project::all();
        return view('project.index',[
            'projects' => $project
        ]);
    }
    public function create(){
        return view('project.create');
    }
    public function store(Request $request){
        try{
            $data = $request->all();
            $project = new Project();
            $project->name = $request->name;
            $project->description = $request->description;
            $project->key = md5($request->name);
            $project->server_api_key = $request->server_api_key;
            $project->save();
            return redirect('/project');
        }catch(Exception $ex){
            dd($ex->getMessage());
        }
    }
    public function edit($key){
        $project = Project::where('key', $key)->first();
        //dd($project);
        return view('/project/edit',[
            'project' => $project
        ]);
    }
    public function update(Request $request){
        $project = Project::where('key', $request->key);
        $project->update(['name' => $request->name,'description' => $request->description, 'server_api_key' => $request->server_api_key]);
        return redirect('/project');
    }
    public function delete($key){
        Project::where('key', $key)->delete();
        return redirect('/project');
    }
}
