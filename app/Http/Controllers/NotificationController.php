<?php

namespace App\Http\Controllers;

use App\FCM;
use App\Project;
use App\Token;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;

class NotificationController extends Controller
{
    public $fcm;
    public function __construct(FCM $fcm)
    {
        $this->fcm = $fcm;
    }
    public function push(Request $request){
//        $dataNotification = [
//            'notification' => [
//                'title' => '',
//                'subtitle' => '',
//                'body' => '',
//            ],
//            'data' => [
//                'key' => 'value',
//            ]
//        ];
        $result['success'] = 0;
        try{
            $dataNotice = $request->data_notice;
            $notification = $dataNotice['notification'];
            $data = $dataNotice['data'];
            $userId = $request->user_id;
            $projectKey = $request->project_key;
            $token = Token::where(['user_id' => $userId ,'project_key' => $projectKey])->first();
            $api_key = Project::where('key', $projectKey)->first();
            if(!$token){
                $result['message'] = 'Information not found.';
                return response()->json($result);
            }
            if(!$token->token_firebase){
                $result['message'] = 'Token not found.';
                return response()->json($result);
            }
            $to = $token->token_firebase;
            $noticeResult = $this->fcm->config($api_key->server_api_key)
                ->to($to)
                ->notificationOwn($userId, $projectKey)
                ->notification($notification)
                ->data($data)
                ->push();
            $result['success'] = 1;
            $result['message'] = 'Notification successfully.';
            $result['data'] = $noticeResult;
            return response()->json($result);
        }catch(Exception $ex){
            $result['message'] = 'Has an error while processing, please try again later';
            return response()->json($result);
        }
    }
}
