<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Token;
use App\FCM;
use Mockery\CountValidator\Exception;


class TokenController extends Controller
{
    public $token;
    public $fcm;
    public function __construct(Token $token, FCM $fcm){
        $this->token = $token;
        $this->fcm = $fcm;
    }
    public function store(Request $request){
        try{
            $result['success'] = 0;
            $token = $this->token->where(['user_id' => $request->user_id, 'project_key' => $request->project_key]);
            if($token->count() != 0){
                $token->update(['token_firebase' => $request->token_firebase]);
                $result['success'] = 1;
                $result['message'] = 'Update token successfully.';
            }else{
                $token->create(['user_id' => $request->user_id, 'project_key' => $request->project_key, 'token_firebase' => $request->token_firebase]);
                $result['success'] = 1;
                $result['message'] = 'Create token successfully.';
            }
            return response()->json($result);
        }catch(Exception $ex){
            $result['success'] = 0;
            $result['message'] = 'Has an error while processing, please try again later';
            return response()->json($result);
        }

    }
}
