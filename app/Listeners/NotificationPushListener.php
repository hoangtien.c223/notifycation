<?php

namespace App\Listeners;

use App\Events\NotificationPush;
use App\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationPushListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NotificationPush $event)
    {
        $data = $event->notificationData;
        $notification = new Notification();
        $notification->title = isset($data->notification['title']) ? $data->notification['title'] : 'Notification title';
        $notification->subtitle = isset($data->notification['subtitle']) ? $data->notification['subtitle'] : 'Notification subtitle';
        $notification->body = isset($data->notification['body']) ? $data->notification['body'] : 'Notification body';
        $notification->icon = isset($data->icon) ? $data->icon : '';
        $notification->click_action = isset($data->click_action) ? $data->click_action : '';
        $notification->priority = isset($data->priority) ? $data->priority : '';
        $notification->sound = isset($data->sound) ? $data->sound : '';
        $notification->data = isset($data->data) ? json_encode($data->data) : '';
        $notification->to = isset($data->recipients) ? $data->recipients : '';
        $notification->user_id = isset($data->user_id) ? $data->user_id : '';
        $notification->project_key = isset($data->project_key) ? $data->project_key : '';
        $notification->save();
    }
}
