<?php

namespace App;

use App\Events\NotificationPush;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Event;

class FCM extends Model
{
    /**
     * Object send
     */
    public $client;
    /**
     * FCM Server url
     */
    public $server_url;
    /**
     * FCM Server key
     */
    public $server_key;
    /**
     * Header push notification;
     */
    public $header;
    /**
     * recipients
     * To topics
     * To single
     */
    public $recipients = '';
    /**
     * Notification info
     */
    public $notification = [];
    /**
     * Data notification
     */
    public $data = [];
    /**
     * Device type
     */
    public $type = 'ios';
    /**
     * Device sound
     */
    public $sound = true;
    /**
     * Device priority
     */
    public $priority = 'high';
    /**
     * Icon
     */
    public $icon = '';
    /**
     * Click action
     */
    public $click_action = '';
    /**
     * user_id
    */
    public $user_id = '';
    /**
     * project_id
     */
    public $project_key = '';

    public function __construct(Client $client, $url = null, $key = null){
        $this->client = $client;
        $this->server_url = $url == null ? env('FCM_SERVER_URL') : $url;
        $this->server_key = $key == null ? env('FCM_SERVER_KEY') : $key;
        return $this;
    }
    /**
     * Set server url & server key
     * @param $url
     * @param $key
     * @return void
    */
    public function config($key = null, $url = null){
        if($url != null){
            $this->server_url = $url;
        }
        if($key != null){
            $this->server_key = $key;
        }
        return $this;
    }
    /**
     * Target;
     * @param topic or token device;
     * @return object;
     */
    public function to($recipients){
        $this->recipients = $recipients;
        return $this;
    }
    /**
     * Notification;
     * @param $notification[] title, subtitle, body;
     * @return object;
     */
    public function notification($notification){
        $this->notification = $notification;
        return $this;
    }
    /**
     * Data notification;
     * @param $data to send;
     * @return object;
     */
    public function data($data){
        $this->data = $data;
        return $this;
    }
    /**
     * Device type;
     * @param web, ios or android;
     * @return object;
     */
    public function device($type){
        $this->type = $type;
        return $this;
    }
    /**
     * notification sound;
     * @param true or false;
     * @return object;
     */
    public function sound($bool = false){
        $this->sound = $bool;
        return $this;
    }
    /**
     * Priority;
     * @param hight;
     * @return object;
     */
    public function priority($high){
        $this->priority = $high;
        return $this;
    }
    /**
     * Set notification icon;
     * @param $icon url;
     * @return object;
     */
    public function icon($icon){
        $this->icon = $icon;
        return $this;
    }
    /**
     * Click notification action;
     * @param action;
     * @return object;
     */
    public function clickAction($action){
        $this->click_action = $action;
        return $this;
    }
    public function notificationOwn($userId, $projectKey){
        $this->user_id = $userId;
        $this->project_key = $projectKey;
        return $this;
    }
    /**
     * Data send structure for device;
     * @param web, ios or android;
     * @return object;
     */
    public function notificationStructure(){
        switch($this->type){
            case 'ios':
            case 'android':
                $structure = [
                    "title" => $this->notification['title'],
                    "subtitle" => $this->notification['subtitle'],
                    "body" => $this->notification['body'],
                    "icon" => $this->icon,
                    "click_action"=> $this->click_action,
                    "data" => $this->data,
                    "sound" => $this->sound,
                    "priority" => $this->priority
                ];
                break;
            default:
                $structure = [
                    "title" => $this->notification['title'],
                    "body" => $this->notification['body'],
                    "icon" => $this->icon,
                    "click_action"=> $this->click_action,
                    "data" => $this->data
                ];
                break;
        }
        return $structure;
    }
    /**
     * Push notification;
     * @param data setup;
     * @return result;
     */
    public function push(){
        $request = $this->client->post($this->server_url,[
            'headers' => [
                'Authorization' => 'key=' . $this->server_key,
                'Content-type' => 'application/json',
            ],
            'json' => [
                "notification" => $this->notificationStructure(),
                "to" => $this->recipients
            ]
        ]);
        $result = json_decode($request->getBody()->getContents(),true);
        if($result['success'] == 1){
            event(new NotificationPush($this));
        }
        return response()->json($result);
    }

}
