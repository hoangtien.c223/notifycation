jQuery(function($){
 $('.ajax-field').on('change', function(){
  var data = $(this).parents('.filter-asset').serializeArray();
  let origin = window.location.origin;
  let path = window.location.pathname;
  if(path != '/'){
   origin += path;
  }
  let qt = '?';
  for(let i in data){
   qt += data[i]['name']+'='+data[i]['value']+'&';
  }
  qt = qt.substr(0, (qt.length) - 1);
  window.location = origin+qt;
 });
});